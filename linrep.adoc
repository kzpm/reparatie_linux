:toc: left
:toc-title: Inhoud
:rep: https://kb.kbase.nl/replinux/debian_GUI.ova
:icons: font

== Reparatie Linux Periode 1


=== Vooraf
Voer de opdrachten uit, zodat je je cijfer voor Linux in P1 op kunt halen.

=== Opdrachten in het kort
1. Download disk image van Debian 11
2. Importeer het bestand in VMWare of VirtualBox
3. Zet kopieren van host naar guest en omgekeerd aan
4. Installeer de VMWare guest additions cd 
5. Reboot je nieuwe image
6. Log in met `root` en wachtwoord `Welkom123#`
7. Wijzig de hostname van `template` naar je eigen voornaam plus de eerste letter van je achternaam.
8. Installeer Openbox window-manager, firefox-esr, terminator, conky en hsetroot
9. Maak gebruikers aan
10. Maak een nieuwe groep aan
11. Voeg gebruikers toe aan de nieuwe groep
12. Maak directories aan
13. Maak bestanden in enkele directories aan
14. Wijzig bestandspermissies
15. Wijzig het openbox opstart bestand
16. Wijzig het conkyrc bestand
17. Grafische desktop starten
18. History inleveren op in ELO

WARNING: Een `#` als prompt betekent: uitvoeren met sudo of als root


=== 1. Download disk image van Debian 11

{rep}[Download debian 11 template ova]

=== 2. Importeer het *ova bestand, wat je gedownload hebt in VMWare of VirtualBox
https://youtu.be/WY11A-eyJWY?t=92[Instructievideo importeren ova bestand]

=== 3. Zet kopieren van host naar guest en omgekeerd aan

- Start de machine, die je net geimporteerd hebt.
- Gebruikersnaam is `root` en het wachtwoord is `Welkom123#`

https://youtu.be/3TgszrGV8ok?t=27[Instructievideo kopieren van host naar guest en omgekeerd]

=== 4. Installeer de VMWare guest additions 
https://youtu.be/klnGGNbkybQ?t=18[Instructie guest additions Debian]


=== 5. Reboot je nieuwe image
- reboot je machine: `systemctl reboot`


=== 6. Log in je nieuwe machine
Gebruikersnaam is `root` en het wachtwoord is `Welkom123#`


=== 7. Wijzig de hostname van `template` naar je eigen voornaam plus de eerste letter van je achternaam.
Dus wanneer je *Jan de Vries* heet, wordt je hostname `jandv`

`# hostnamectl set-hostname <jouw_naam>`

Typ nu `exit` en log opnieuw in. Je hostname is nu gewijzigd.
*Maak hier een screenshot van!*
 
=== 8. Installeer Openbox window-manager, firefox-esr, terminator, conky en hsetroot
`# apt update`

`# apt install openbox xorg xinit firefox-esr terminator conky hsetroot`

=== 9. Maak nieuwe gebruikers aan

`# useradd -m ryzen` +
`# useradd -m snapdragon` +
`# useradd -m xeon`

=== 10. Maak een nieuwe groep aan
`# groupadd processors`

=== 11. Voeg gebruikers toe aan de nieuwe groep
`# usermod -a -G processors ryzen`

=== 12. Maak directories aan
`# mkdir -p /home/ryzen/amd /home/snapdragon/qualcomm /home/xeon/intel`

=== 13. Maak bestanden in de nieuwe directories aan
`# touch /home/ryzen/amd/amdtext` +
`# touch /home/snapdragon/qualcomm/snapdragontext` +
`# touch /home/xeon/intel/xeontext`

=== 14. Wijzig bestandspermissies en eigenaar
`# chown -R ryzen:ryzen /home/ryzen` +
`# chown -R snapdragon:snapdragon /home/snapdragon` +
`# chown -R xeon:xeon /home/xeon` 
 
=== 15. Wijzig het openbox opstart bestand

`# nano /etc/xdg/openbox/autostart`

Plak de onderstaande regels aan het eind van het bestand:

 hsetroot -solid "#xxyyzz"

 conky -q &

Wijzig de 'xx', de 'yy' en de 'zz' door een hexadecimale waarde. Je mag dus de cijfers 0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f gebruiken.
Voorbeeld:
 hsetroot -solid "#a2f084"

*Kopieer nu de kleurcode. Deze heb je straks nodig!+*
Sla het bestand daarna op met *CTRL+O* en daarna *CTRL+X*

=== 16. Wijzig het `conky.conf` bestand

`# sed -i 's|Info:|<jouw_naam> |g' /etc/conky/conky.conf`

Voorbeeld:

`# sed -i 's|Info:|Jan de Vries |g' /etc/conky/conky.conf`


=== 17. Je eigen GUI starten met `startx`

- Start je grafische desktop, die je zelf hebt gemaakt met het commando `startx`
- Klik met de rechtermuisknop ergens op je bureaublad
- Start de firefox browser en surf naar het noorderportal
- Login op je schoolaccount
- Start It's Learning (ELO)
- Ga naar de tegel *T4_21-N3-1 OS Linux* en klik in P1 op de link 

=== 18. Stuur je `history` op

- Voer het commando `history > history.txt` uit in een terminal (rechtsklikken `xterm`)
- Lever bij de inleverlink voor de reparatieopdracht het volgende in:

1. `history.txt`
2. Screenshot van je bureaublad waarin de hostname te zien is
3. Screenshot van het MAC adress van je wireless interface (`ip a`)






